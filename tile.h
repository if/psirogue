#ifndef __TILE_H__
#define __TILE_H__

#include "common.h"

/* tile property flags */
#define TILE_NOTHING 0
#define TILE_BARRIER shl(0)


/* data for a tile (a cell on the map) */
typedef struct {
    int ch;
    int flags;
    const char *desc;
} tile;


// declare tile types, defined in tile.c
extern tile tile_floor;
extern tile tile_wall_h;
extern tile tile_wall_v;
extern tile tile_wall_tl;
extern tile tile_wall_tr;
extern tile tile_wall_bl;
extern tile tile_wall_br;

#endif
