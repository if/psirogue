#include "tile.h"

// tile definitions
tile tile_floor = {
    '.', TILE_NOTHING,
    "the floor",
};

tile tile_wall_h = {
    '-', TILE_BARRIER,
    "a wall",
};
tile tile_wall_v = {
    '|', TILE_BARRIER,
    "a wall",
};

tile tile_wall_tl = {
    '+', TILE_BARRIER,
    "a wall",
};

tile tile_wall_tr = {
    '+', TILE_BARRIER,
    "a wall",
};

tile tile_wall_bl = {
    '+', TILE_BARRIER,
    "a wall",
};

tile tile_wall_br = {
    '+', TILE_BARRIER,
    "a wall",
};
