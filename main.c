#include <stdio.h>
#include <time.h>
#include "include/BearLibTerminal.h"
#include "common.h"
#include "game.h"

int main() {
    // seed the RNG
    srand(time(NULL));

    if (!terminal_open()) {
        fprintf(stderr, "failed to open terminal.\n");
        return 1;
    }

    // configure the terminal
    char buf[50];
    snprintf(buf, 50, "window: size=%dx%d",
             WINDOW_WIDTH, WINDOW_HEIGHT);
    terminal_set(buf);
    terminal_set("font: DejaVuSansMono.ttf, size=8");
    terminal_refresh();

    // run the game
    game game;
    init_game(&game);
    
    while (true) {
        if (has_quit(&game))
            break;

        run_game(&game);
        terminal_refresh();
    }

    end_game(&game);
    terminal_close();
}
