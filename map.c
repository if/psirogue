#include <stdlib.h>
#include "map.h"



/** allocate a new map, return a pointer to it */
map *new_map() {
    map *the_map = malloc(sizeof(map));
    
    for (int i = 0; i < MAP_WIDTH; i++)
        for (int j = 0; j < MAP_HEIGHT; j++)
            the_map->items[i][j] = NULL;
    
    for (int i = 0; i < MAP_WIDTH; i++)
        for (int j = 0; j < MAP_HEIGHT; j++)
            the_map->entities[i][j] = NULL;
    
    return the_map;
}

/** free all memory used by a map */
void delete_map(map *map) {
    // free all the entities and items
    for (int x = 0; x < MAP_WIDTH; x++) {
        for (int y = 0; y < MAP_HEIGHT; y++) {
            entity *e = map->entities[x][y];
            // player delete is handled by game
            if (e != NULL &&
                !(e->flags & ENTITY_PLAYER))
                delete_entity(e);

            item *i = map->items[x][y];
            if (i != NULL)
                delete_item(i);
        }
    }
            
    free(map);
}

/** generate content for a map */
void fill_map(map *map) {
    // currently this just makes a "dummy" map
    // that isn't procgen'd and is set up to
    // make testing stuff easier
    for (int x = 0; x < MAP_WIDTH; x++) {
        for (int y = 0; y < MAP_HEIGHT; y++) {
            if (x == 0 || x == MAP_WIDTH-1)
                map->tiles[x][y] = tile_wall_v;
            else if (y == 0 || y == MAP_HEIGHT-1)
                map->tiles[x][y] = tile_wall_h;
            else 
                map->tiles[x][y] = tile_floor;
        }
    }

    map->tiles[0][0] = tile_wall_tl;
    map->tiles[0][MAP_HEIGHT-1] = tile_wall_bl;
    map->tiles[MAP_WIDTH-1][0] = tile_wall_tr;
    map->tiles[MAP_WIDTH-1][MAP_HEIGHT-1] =
        tile_wall_br;

    
    for (int x = 0; x < MAP_WIDTH; x++)
        for (int y = 0; y < MAP_HEIGHT; y++)
            map->entities[x][y] = NULL;

    map->entities[10][10] = new_entity(entity_gob);
    map->items[5][10] = new_item(item_sword);
}

/** update the map (cleanup, fun terrain stuff, &c) */
void update_map(map *map) {
    for (int x = 0; x < MAP_WIDTH; x++) {
        for (int y = 0; y < MAP_HEIGHT; y++) {
            entity *e = map->entities[x][y];
            if (e != NULL
                && !(e->flags & ENTITY_PLAYER)) {
                // clean up dead entities
                if (e->flags & ENTITY_DEAD) {
                    delete_entity(e);
                    map->entities[x][y] = NULL;
                }
            }
        }
    }
}



/** return whether coords are within bounds */
bool chk_coords(int x, int y) {
    return (x >= 0 && x < MAP_WIDTH
            && y >= 0 && y < MAP_HEIGHT);
}

/** try to place an entity on the map, and
   return whether the placement worked.
   this is for *initially* placing entities,
   e.g. when a level is being populated. */
bool place_entity(map *map, int x, int y, entity *e) {
    if (!chk_coords(x, y))
        return false;

    // place entity and update its own location
    map->entities[x][y] = e;
    e->x = x;
    e->y = y;

    return true;
}

/** get the entity at (x, y) (may be null) */
entity *entity_at(map *map, int x, int y) {
    if (chk_coords(x, y))
        return map->entities[x][y];
    else return NULL;
}


/** place an item in cell (x, y). returns true if
 item was placed ((x, y) must be a valid location 
 and unoccupied by other items). */
bool place_item(map *map, int x, int y, item *i) {
    if (chk_coords(x, y) && map->items[x][y] == NULL) {
        map->items[x][y] = i;
        return true;
    }

    return false;
}

/** get the item at (x, y) (maybe be null) */
item *item_at(map *map, int x, int y) {
    if (chk_coords(x, y))
        return map->items[x][y];
    else return NULL;
}

/** like `item_at', but also removes the item from
 its location on the map */
item *take_item(map *map, int x, int y) {
    item *i = item_at(map, x, y);

    if (i != NULL)
        map->items[x][y] = NULL;

    return i;
}
