#ifndef __MAP_H__
#define __MAP_H__

#define MAP_WIDTH 20
#define MAP_HEIGHT 20

#include "common.h"
#include "tile.h"
#include "entity.h"
#include "item.h"

typedef struct {
    tile tiles[MAP_WIDTH][MAP_HEIGHT];
    item *items[MAP_WIDTH][MAP_HEIGHT];
    entity *entities[MAP_WIDTH][MAP_HEIGHT];
} map;

map *new_map();
void delete_map(map *map);
void fill_map(map *map);
void update_map(map *map);
bool chk_coords(int x, int y);
bool place_entity(map *map, int x, int y, entity *e);
entity *entity_at(map *map, int x, int y);
item *item_at(map *map, int x, int y);
bool place_item(map *map, int x, int y, item *i);
item *take_item(map *map, int x, int y);

#endif
