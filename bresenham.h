#ifndef __BRESENHAM_H__
#define __BRESENHAM_H__

/** a cell position */
typedef struct {
    int x, y;
} cell;

typedef struct {
    cell *cells;
    int num_cells;
} b_line;

b_line *bresenham_line(int x0, int y0,
                       int x1, int y1);
void free_b_line(b_line *line);

#endif
