#ifndef __ENTITY_H__
#define __ENTITY_H__

#include "common.h"

// entity trait flags
#define ENTITY_NOTHING 0
#define ENTITY_DEAD shl(0)
#define ENTITY_PLAYER shl(1)


/* an entity (monster, etc) */
typedef struct {
    char ch;
    const char *name;
    int flags;
    int x, y;
    float hp, max_hp;
    float regen; // hp regen per tick
    float dmg; // attack damage
    float df; // % chance of blocking hit
} entity;


entity *new_entity(entity e);
void delete_entity(entity *e);
void update_entity(entity *e);
void attack(entity *attacker, entity *victim);

// entity predecls
extern entity entity_player;
extern entity entity_gob;

#endif
