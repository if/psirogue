* ideas
** silicon genes
- monsters have "genes" corresponding to bit-flags
- genes are shown on monster info screen
  - indicate editable sectors || known fields
    - editable = via color
    - or, unknown = ** or something
  - example
    bf 02 c2 35 ** ** **
- you can use magic to edit genes
- gene ideas
  - living shield
    - monster absorbs damage nearby monsters suffer
    - such monsters have really high health
    - (usually) can't attack, just evade
  - mutator
    - can mutate nearby monsters
    - mutates monsters into distinctive flavor
** use of color
- desaturated (gray) = not in FOV
- magic bolts are colorful
- so are genes
- rest is black/white/gray
* miscellaneous FIXME/TODO items
** DONE correctly handle TK_CLOSE
** DONE numpad controls
* things to do, in order
** DONE game states/menus
** DONE "fake" mapgen
 - have mapgen functionality in place to make a "fake" map for testing
 - actual procgen is done later
** DONE player movement
** DONE camera
** DONE fighting
 - make "training dummy" entities
 - health ://
** DONE [3/3] UI stuff
 - [X] [1/1] pop-up windows
   - [X] confirmation dialogs
 - [X] health, mana
 - [X] messages (running into stuff, combat, etc)
** DONE [2/2] inventory / basic items
- [X] items
- [X] inventory screen
  - i (view), d (drop), u (use), r (remove)
** TODO bresenham
** TODO simple spells
** TODO effect timing system
- effects can "request time" from the game
  - put on a queue!
- game keeps track of largest (/not/ cumulative) active request
 - => timer field
- framerate is controlled, game is notified of each tick
- when timer != 0, timer counts down each tick
- input is taken again when timer == 0
** TODO lighting and FOV
** TODO monster inspection
- [ ] for monsters
  - <name> <genes>
  - at bottom of left sidebar
- put nearest in list
  - go thru list with <TAB>/<BACKTAB>
  - avoids arrow/number navigation
- use a bsp or something?
- part of GAME_PLAY mode
** TODO basic tiles
** TODO monster ai/pathfinding/etc
** TODO mapgen
** TODO saving/loading
** TODO finish "content" (items, monsters, tiles, &c)
** TODO level progression

