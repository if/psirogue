#ifndef __GAME_H__
#define __GAME_H__

#include "common.h"
#include "map.h"
#include "entity.h"
#include "player.h"


/* the current "screen" the user is on */
typedef enum {
    GAME_MENU,
    GAME_PLAYING,
    GAME_QUIT,
    GAME_CONFIRM,
    GAME_INVENTORY,
} game_state;

/* the action being taken in the inventory */
typedef enum {
    INV_NONE,
    INV_REMOVE,
    INV_USE,
    INV_DROP,
} inventory_mode;


/* holds all the game's data */
typedef struct {
    game_state state;
    /* TODO: use stack if state gets complex */
    game_state prev_state;
    game_state queued_state;
    map *map;
    player *player;
    inventory_mode inv_mode;
} game;


void init_game(game *the_game);
void end_game(game *game);
void run_game(game *game);
bool has_quit(game *game);


#endif
