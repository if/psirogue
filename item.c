#include <stdlib.h>
#include "item.h"


item item_sword = {
    .ch = '/', .name = "sword",
    .flags = ITEM_WEAPON,
    .stats = { .dmg = 5.0 },
};


/** allocate a new item based off a prototype. */
item *new_item(item i) {
    item *the_item = malloc(sizeof(item));
    the_item->ch = i.ch;
    the_item->name = i.name;
    the_item->flags = i.flags;
    the_item->stats = i.stats;

    return the_item;
}

/** delete a dynamically allocated item. */
void delete_item(item *i) {
    if (i != NULL)
        free(i);
}
