#ifndef __COMMON_H__
#define __COMMON_H__

#define WINDOW_WIDTH 80
#define WINDOW_HEIGHT 25

// left shift macro
// -- useful for making bit fields
#define shl(x) (1 << x)

// color definitions (aarrggbb)
#define COLOR_BLACK  0xff000000
#define COLOR_WHITE  0xfffffff0
#define COLOR_PURPLE 0xff403047
#define COLOR_PINK   0xfff0b7f0

#include <stdio.h>
#include <stdbool.h>

#endif
