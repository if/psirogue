#ifndef __ITEM_H__
#define __ITEM_H__

#include "common.h"

#define ITEM_WEAPON shl(1)


typedef union {
    float dmg;
} item_stats;

typedef struct {
    char ch;
    int flags;
    const char *name;
    item_stats stats;
} item;

item *new_item(item i);
void delete_item(item *i);


extern item item_sword;

#endif
