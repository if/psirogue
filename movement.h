#ifndef __MOVEMENT_H__
#define __MOVEMENT_H__

#include "map.h"
#include "entity.h"
#include "player.h"
#include "common.h"

bool move_ai(map *map, entity *e);
bool move_player(map *map, player *p, int key);
bool try_move(map *map, int x, int y, entity *e);
bool can_move(map *map, int x, int y, entity *e);


#endif
