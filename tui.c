#include <stdio.h>
#include "include/BearLibTerminal.h"
#include "tui.h"
#include "common.h"


/** printf a message onto the terminal */
void print_fmt(int x, int y, const char *fmt, ...) {
    va_list argp;
    va_start(argp, fmt);

    // sprintf the format string
    char msg[256];
    vsnprintf(msg, 256, fmt, argp);
    
    va_end(argp);

    // print msg to the terminal
    terminal_print(x, y, msg);
}

/** printf a message onto the log area */
void print_msg(const char *fmt, ...) {
    va_list argp;
    va_start(argp, fmt);

    // sprintf the format string
    char msg[256];
    vsnprintf(msg, 256, fmt, argp);
    
    va_end(argp);

    // clear the log area
    terminal_clear_area(10, 1, WINDOW_WIDTH-10, 1);
    // print msg to the terminal
    terminal_put(1, 1, '>');
    terminal_print(3, 1, msg);
}

/** print a ratio, for things like health.*/
void print_ratio(int x, int y, float amt, float max) {
    char buf[8];
    
    snprintf(buf, 8, "%0.1f/%0.1f", amt, max);
    terminal_print(x, y, buf);
}
