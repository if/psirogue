#include <stdlib.h>
#include "entity.h"
#include "tui.h"

/** entity prototypes */
entity entity_player = {
    .ch ='@',
    .name = "player",
    .flags = ENTITY_PLAYER,
    .hp = 20.0f, .max_hp = 20.0f,
    .regen = 0.2f,
    .dmg = 2.0f, .df = 0,
};

entity entity_gob = {
    .ch ='g',
    .name = "goblin",
    .flags = ENTITY_NOTHING,
    .hp = 20.0f, .max_hp = 20.0f,
    .regen = 0.1f,
    .dmg = 2.0f, .df = 20,
};




/** create a new entity from a prototype */
entity *new_entity(entity e) {
    entity *the_entity = malloc(sizeof(entity));
    the_entity->ch = e.ch;
    the_entity->name = e.name;
    the_entity->flags = e.flags;
    the_entity->x = e.x;
    the_entity->y = e.y;
    the_entity->hp = e.hp;
    the_entity->max_hp = e.max_hp;
    the_entity->regen = e.regen;
    the_entity->dmg = e.dmg;
    the_entity->df = e.df;

    return the_entity;
}

/** clean up resources used by entity */
void delete_entity(entity *e) {
    free(e);
}

/** update entity state by 1 tick */
void update_entity(entity *e) {
    // ~heal~
    if (e->hp < e->max_hp)
        e->hp += e->regen;
    if (e->hp > e->max_hp)
        e->hp = e->max_hp;
}

/** attack another entity */
void attack(entity *attacker, entity *victim) {
    bool blocked = (rand() % 100) <= victim->df;

    if (!blocked) {
        victim->hp -= attacker->dmg;
        
        if (attacker->flags & ENTITY_PLAYER)
            print_msg("you hit the %s.", victim->name);
        else if (victim->flags & ENTITY_PLAYER)
            print_msg("the %s hits you.",
                      attacker->name);

        if (victim->hp <= 0) {
            victim->flags |= ENTITY_DEAD;
            print_msg("the %s died.", victim->name);
        }
    }
}
