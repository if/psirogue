CC = gcc
CFLAGS = -Wall
LFLAGS = -lBearLibTerminal -Llib -Iinclude -lGL -lm
EXEC = psi
SOURCES = *.c


$(EXEC): $(SOURCES)
	$(CC) $^ $(CFLAGS) $(LFLAGS) -o $@


clean:
	rm -vf $(EXEC) *.o
