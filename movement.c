#include "include/BearLibTerminal.h"
#include "movement.h"
#include "tui.h"

void move(map *map, int x, int y, entity *e);
bool check_for_item(map *map, player *p);


/** move to a location chosen by the entity's
    """artificial intelligence""". returns whether
    the entity was able to move. */
bool move_ai(map *map, entity *e) {
    return false;
}

/** move based on keyboard controls. returns
    whether the player was able to move. */
bool move_player(map *map, player *p, int key) {
    entity *e = p->ent;
    bool moved = false;
    int x = e->x, y = e->y;

    switch (key) {
    case TK_8:
    case TK_KP_8:
    case TK_UP:
        moved = try_move(map, x, y-1, e);
        break;
    case TK_2:
    case TK_KP_2:
    case TK_DOWN:
        moved = try_move(map, x, y+1, e);
        break;
    case TK_4:
    case TK_KP_4:
    case TK_LEFT:
        moved = try_move(map, x-1, y, e);
        break;
    case TK_6:
    case TK_KP_6:
    case TK_RIGHT:
        moved = try_move(map, x+1, y, e);
        break;
    case TK_7:
    case TK_KP_7:
        moved = try_move(map, x-1, y-1, e);
        break;
    case TK_9:
    case TK_KP_9:
        moved = try_move(map, x+1, y-1, e);
        break;
    case TK_1:
    case TK_KP_1:
        moved = try_move(map, x-1, y+1, e);
        break;
    case TK_3:
    case TK_KP_3:
        moved = try_move(map, x+1, y+1, e);
        break;
        
    default:
        break;
    }

    if (moved) check_for_item(map, p);
    
    return moved;
}

/** check for an item in the current cell, and
    try to pick it up if there is one. returns
    whether an item was collected. */
bool check_for_item(map *map, player *p) {
    int x = p->ent->x, y = p->ent->y;
    item *i = take_item(map, x, y);
    
    if (i != NULL) {
        bool added = add_to_inventory(p, i);
        // if not added (ie not enough space)
        // then "put the item back" into the cell
        if (!added) {
            place_item(map, x, y, i);
        } else {
            print_msg("you pick up a %s.", i->name);
            return true;
        }
    }

    return false;
}


/** try to move an entity. returns whether
    the move was a success. */
bool try_move(map *map, int x, int y, entity *e) {
    bool is_clear = can_move(map, x, y, e);

    if (is_clear) {
        move(map, x, y, e);
    } else if (entity_at(map, x, y) != NULL) {
        attack(e, entity_at(map, x, y));
    }

    return is_clear;
}

/** return whether an entity can move to the
    specified location. */
bool can_move(map *map, int x, int y, entity *e) {
    tile t = map->tiles[x][y];

    // these are all the conditions that have
    // to be met for the movement to occur
    bool not_barrier = !(t.flags & TILE_BARRIER);
    bool in_bounds = chk_coords(x, y);
    bool unoccupied = (entity_at(map, x, y) == NULL);

    return (not_barrier && in_bounds && unoccupied);
}

/** move an entity to a new location. doesn't
    perform any checks. */
void move(map *map, int x, int y, entity *e) {
    map->entities[e->x][e->y] = NULL;
    map->entities[x][y] = e;
    e->x = x;
    e->y = y;
}
