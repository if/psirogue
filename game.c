#include <stdlib.h>
#include "include/BearLibTerminal.h"
#include "game.h"
#include "common.h"
#include "movement.h"
#include "tui.h"
#include "bresenham.h" // TODO: no

// "private" stuff
static void run_state_play(game *game);
static void run_state_menu(game *game);
static void run_state_confirm(game *game);
static void run_state_inventory(game *game);
static void switch_state(game *game, game_state st);
static void play_draw_world(game *game);
static void play_draw_ui(game *game);
static void play_handle_input(game *game, int key);
static void draw_cell(map *map, int cx, int cy,
                      int x, int dy);
static void confirm_dialog(game *game, const char *msg,
                           game_state new_state);
static bool do_inventory_action(game *game, char ch);


/** initialize a new game */
void init_game(game *the_game) {
    the_game->player = new_player();
    
    the_game->map = new_map();
    fill_map(the_game->map);
    place_entity(the_game->map, 6, 6,
                 the_game->player->ent);

    the_game->prev_state = GAME_MENU;
    switch_state(the_game, GAME_MENU);

    the_game->inv_mode = INV_NONE;
}

/** clean up memory used by a game */
void end_game(game *game) {
    delete_map(game->map);
    delete_player(game->player);
}
 
/** return whether the user has quit the game */
bool has_quit(game *game) {
    return (game->state == GAME_QUIT);
}

/** draw game contents (map, stats, etc) */
void run_game(game *game) {
    /* immediately quit if window is closed */
    if (terminal_has_input()
        && terminal_peek() == TK_CLOSE) {
        switch_state(game, GAME_QUIT);
        return;
    }
    
    switch (game->state) {
    case GAME_MENU:
        run_state_menu(game);
        break;
    case GAME_PLAYING:
        run_state_play(game);
        break;
    case GAME_CONFIRM:
        run_state_confirm(game);
        break;
    case GAME_INVENTORY:
        run_state_inventory(game);
        break;
        
    default:
        break;
    }
}

/** run the game in play state */
void run_state_play(game *game) {
    if (terminal_has_input()) {
        play_handle_input(game, terminal_read());
        // update screen on input
        play_draw_world(game);
        play_draw_ui(game);
        update_map(game->map);
    }
}

/** draw the world during play state */
void play_draw_world(game *game) {
    // half of camera width and height
    static const int hcw = (WINDOW_WIDTH-14)/2,
        hch = (WINDOW_HEIGHT-4)/2;
    int px = game->player->ent->x,
        py = game->player->ent->y;

    for (int x = 0; x < 2*hcw; x++) {
        for (int y = 0; y < 2*hch; y++) {
            int mx = px - hcw + x,
                my = py - hch + y;
            
            if (chk_coords(mx, my))
                draw_cell(game->map, mx, my,
                          10+x, 2+y);
            else
                terminal_put(10+x, 2+y, ' ');
        }
    }

    /* test (bresenham) */
    b_line *line =
        bresenham_line(px, py, 10, 10);

    for (int i = 0; i < line->num_cells; i++)
        terminal_put(-1+hcw+line->cells[i].x,
                     -7+hch+line->cells[i].y, '#');

    free_b_line(line);
}

/** draw the cell at (mx, my) on the map at
    (x, y) on the terminal */
void draw_cell(map *map, int mx, int my,
               int x, int y) {
    // choose what gets drawn based on
    // which layers have content in them
    char ch = map->tiles[mx][my].ch;

    if (map->entities[mx][my] != NULL)
        ch = map->entities[mx][my]->ch;
    else if (map->items[mx][my] != NULL)
        ch = map->items[mx][my]->ch;

    terminal_put(x, y, ch);
}

/** draw the play state UI */
void play_draw_ui(game *game) {
    // health bars and such
    terminal_print(1, 3, "hp");
    print_ratio(4, 3, game->player->ent->hp,
                game->player->ent->max_hp);
    terminal_print(1, 4, "mana");
    print_ratio(6, 4, game->player->mana,
                game->player->max_mana);
    print_fmt(1, 5, "dmg %.2f",
              game->player->ent->dmg);

    // nano-style shortcut bar
    terminal_print(1, WINDOW_HEIGHT-1,
                   "<esc> menu"
                   "  <q> quit");    
}

/** handle input during play state */
void play_handle_input(game *game, int key) {
    switch (key) {
    case TK_ESCAPE:
        confirm_dialog(game, "return to main menu?",
                       GAME_MENU);
        break;
    case TK_Q:
        confirm_dialog(game, "really quit?",
                       GAME_QUIT);
        break;
    case TK_I:
        switch_state(game, GAME_INVENTORY);
        break;
    case TK_R:
        game->inv_mode = INV_REMOVE;
        switch_state(game, GAME_INVENTORY);
        break;
    case TK_U:
        game->inv_mode = INV_USE;
        switch_state(game, GAME_INVENTORY);
        break;
    case TK_D:
        game->inv_mode = INV_DROP;
        switch_state(game, GAME_INVENTORY);
        break;
        
    default:
        // delegate all other keys to player
        move_player(game->map, game->player, key);
        break;
    }
}

/** run the game's menu state */
void run_state_menu(game *game) {
    if (terminal_has_input()) {
        switch (terminal_read()) {
        case TK_N:
            switch_state(game, GAME_PLAYING);
            break;
        case TK_Q:
            switch_state(game, GAME_QUIT);
            break;

        default:
            break;
        }
    }
}


/** display a confirmation dialog for a state change */
void run_state_confirm(game *game) {
    if (terminal_has_input()) {
        switch (terminal_read()) {
        case TK_N:
            switch_state(game, game->prev_state);
            break;
        case TK_Y:
            switch_state(game, game->queued_state);
            break;
            
        default:
            break;
        }
    }
}

/** summon up a confirm dialog for a state change. */
void confirm_dialog(game *game, const char *msg,
                    game_state new_state) {
    switch_state(game, GAME_CONFIRM);
    game->queued_state = new_state;

    terminal_print(1, 1, msg);
    terminal_print(2, 2, "<y> yes");
    terminal_print(2, 3, "<n> no");
}

/** draw the inventory state and take input */
void run_state_inventory(game *game) {
    terminal_print(1, 1, "<r> remove "
                   "<u> use <d> drop");
    
    for (int i = 0,line = 3; i < INVENTORY_SIZE; i++) {
        char c = (char)('a' + i);
        item *it = get_from_inventory(game->player, c);
        
        if (it != NULL) {
            terminal_put(1, line, '(');
            terminal_put(2, line, c);
            terminal_put(3, line, ')');
            terminal_put(5, line, it->ch);
            terminal_print(7, line, it->name);
            line++;
        }
    }

    if (terminal_has_input()) {
        int input = terminal_read();
        
        if (input == TK_ESCAPE) {
            // exit inventory state
            game->inv_mode = INV_NONE;
            switch_state(game, game->prev_state);
        } else if (game->inv_mode == INV_NONE) {
            // set inventory mode, if necessary
            switch(input) {
            case TK_R:
                terminal_print(1, 0, "<remove>");
                game->inv_mode = INV_REMOVE;
                break;
            case TK_U:
                terminal_print(1, 0, "<use>");
                game->inv_mode = INV_USE;
                break;
            case TK_D:
                terminal_print(1, 0, "<drop>");
                game->inv_mode = INV_DROP;
                break;
            default:
                break;
            }
        } else {
            // select an item and do something with it
            char ch = 'a' + (input - 4);
            if (do_inventory_action(game, ch)) {
                // exit inventory state
                game->inv_mode = INV_NONE;
                switch_state(game, game->prev_state);
            }
        }
    }
}

/** perform the current inventory mode on the
    item stored under `ch'. returns whether the
    action occurred successfully. */
bool do_inventory_action(game *game, char ch) {
    item *it = get_from_inventory(game->player, ch);

    if (it == NULL)
        return false;

    switch (game->inv_mode) {
    case INV_REMOVE:
        if (it->flags & ITEM_WEAPON)
            unequip_weapon(game->player, it);
        break;
        
    case INV_USE:
        if (it->flags & ITEM_WEAPON)
            equip_weapon(game->player, it);
        break;
        
    case INV_DROP:
        if (it->flags & ITEM_WEAPON)
            unequip_weapon(game->player, it);
        
        (void)rm_from_inventory(game->player, ch);
        (void)place_item(game->map,
                         game->player->ent->x,
                         game->player->ent->y, it);
        break;
        
    default:
        break;
    }

    return true;
}


/** handle a game state transition */
void switch_state(game *game, game_state st) {
    // save previous state
    game->prev_state = game->state;

    // reset colors, clear screen
    terminal_bkcolor(COLOR_WHITE);
    terminal_color(COLOR_BLACK);
    terminal_clear();
    
    // transition to new state
    switch (st) {
    case GAME_PLAYING:
        // draw initial game map/ui
        play_draw_world(game);
        play_draw_ui(game);
        break;
    case GAME_MENU:
        terminal_print(1, 1, "PSI::ROGUE");
        terminal_print(3, 2, "<n> new game");
        terminal_print(3, 3, "<q> quit");
        break;
    case GAME_QUIT:
        printf("goodbye!\n");
        break;
        
    default:
        break;
    }

    game->state = st;
}
