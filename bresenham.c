#include <stdlib.h>
#include <assert.h>
#include <math.h>
#include "common.h"
#include "bresenham.h"


static b_line *new_b_line(int ncells);
static b_line *get_line(int x0, int y0,
                        int x1, int y1,
                        bool is_high);

// TODO:
// - finish algorithm (add all directions)
// - improve debugging visual in game.c
// - see if you can combine 2 helper fns -> 1
//   - high_line(x0, y0, x1, y1) == low_line(y0, x0, y1, x1)
//     - you just need to make sure right struct fields are
//         being assigned
//     - so maybe add boolean flag for that

/** use bresenham's line algorithm to get a list
    of points along the line from (x, y) to (x1, y1).
    the points are all integers, so they correspond
    to cells on the game map. the return value is
    dynamically allocated and needs a corresponding
    call to `free_line'. */
b_line *bresenham_line(int x0, int y0,
                         int x1, int y1) {
    if (abs(y1 - y0) < abs(x1 - x0)) {
        if (x0 > x1)
            return get_line(x1, y1, x0, y0, false);
        else
            return get_line(x0, y0, x1, y1, false);
    } else {
        if (y0 > y1)
            return get_line(y1, x1, y0, x0, true);
        else
            return get_line(y0, x0, y1, x1, true);
    }

    assert(0);
}


static b_line *new_b_line(int ncells) {
    b_line *line = malloc(sizeof(b_line));
    line->cells = malloc(ncells * sizeof(cell));
    line->num_cells = ncells;

    return line;
}

void free_b_line(b_line *line) {
    free(line->cells);
    free(line);
}

/** create a line from (x0, y0) to (x1, y1) using
    bresenham's line-drawing algorithm. the is_high
    flag indicates whether the line is "rising", 
    ie y0 > y1. */
static b_line *get_line(int x0, int y0,
                        int x1, int y1,
                        bool is_high) {
    int dx = x1 - x0;
    int dy = y1 - y0;
    
    int yi = 1;
    if (dy < 0) {
        dy *= -1;
        yi = -1;
    }

    int d = (2 * dy) - dx;
    int y = y0;
    b_line *line = new_b_line(dx);

    int i = 0;
    for (int x = x0; x < x1; x++, i++) {
        if (is_high) {
            line->cells[i].y = y;
            line->cells[i].x = x;
        } else {
            line->cells[i].x = x;
            line->cells[i].y = y;            
        }

        if (d > 0) {
            y += yi;
            d += 2 * (dy - dx);
        } else {
            d += 2 * dy;
        }
    }

    return line;
}
