#ifndef __PLAYER_H__
#define __PLAYER_H__

#include "common.h"
#include "entity.h"
#include "item.h"

#define INVENTORY_SIZE 26

/* the game's player; basically an extended
   version of an entity. */
typedef struct {
    entity *ent; // hroom hroom
    float mana, max_mana;
    float mana_regen;
    item *inventory[INVENTORY_SIZE];
    item* weapon;
} player;


player *new_player();
void delete_player(player *p);

bool add_to_inventory(player *p, item *i);
item *get_from_inventory(player *p, char c);
item *rm_from_inventory(player *p, char c);

void equip_weapon(player *p, item *w);
void unequip_weapon(player *p, item *w);


#endif
