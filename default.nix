with import <nixpkgs> {};

stdenv.mkDerivation {
  name = "psirogue";
  version = "0.1";
  src = ./.;
  nativeBuildInputs = [ pkg-config gnumake ];
  buildInputs = [
    bearlibterminal
    xorg.libX11
    libGL
  ];
  
  inherit bearlibterminal;

  # buildPhase = ''
  #   gcc -I. -L. -Wl,-rpath,. main.c -lBearLibTerminal -o psi
  # '';

  installPhase = ''
    mkdir -p $out/bin
    cp psi $out/bin
  '';
}
