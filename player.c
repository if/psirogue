#include <stdlib.h>
#include "player.h"

/** create a new player */
player *new_player() {
    player *the_player = malloc(sizeof(player));
    the_player->ent = new_entity(entity_player);
    the_player->max_mana = 5;
    the_player->mana = the_player->max_mana;
    the_player->mana_regen = 0.2;
    the_player->weapon = NULL;

    for (int i = 0; i < INVENTORY_SIZE; i++)
        the_player->inventory[i] = NULL;

    return the_player;
}

/** clean up resources used by player */
void delete_player(player *p) {
    delete_entity(p->ent);

    for (int i = 0; i < INVENTORY_SIZE; i++)
        if (p->inventory[i] != NULL)
            delete_item(p->inventory[i]);

    free(p);
}


/** equip a weapon */
void equip_weapon(player *p, item *w) {
    if (w == NULL || !(w->flags & ITEM_WEAPON)
        || p->weapon == w)
        return;
    
    // unequip current weapon
    if (p->weapon != NULL)
        unequip_weapon(p, p->weapon);

    p->ent->dmg += w->stats.dmg;
    p->weapon = w;
}

/** unequip a weapon */
void unequip_weapon(player *p, item *w) {
    if (w == NULL || !(w->flags & ITEM_WEAPON)
        || p->weapon != w)
        return;

    p->ent->dmg -= w->stats.dmg;
    p->weapon = NULL;
}


/** add an item to the inventory. returns true if
    the item was successfully added. */
bool add_to_inventory(player *p, item *it) {
    // find the first "open" inventory slot
    for (int i = 0; i < INVENTORY_SIZE; i++)
        if (p->inventory[i] == NULL) {
            p->inventory[i] = it;
            return true;
        }

    return false;
}

/** get an item from the given inventory slot.
    if the slot is empty or out of bounds, this
    will return NULL. if the slot is out of bounds,
    it will also print an error message. */
item *get_from_inventory(player *p, char c) {
    if (c >= 'a' && c <= 'z') {
        return p->inventory[c - 'a'];
    } else {
        fprintf(stderr,
                "bad inventory index: %d\n", c);
    }

    return NULL;
}

/** like `get_from_inventory', but also removes
    the item (if it exists) from the inventory. */
item *rm_from_inventory(player *p, char c) {
    item *i = get_from_inventory(p, c);
    p->inventory[c - 'a'] = NULL;

    return i;
}
