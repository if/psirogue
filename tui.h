#ifndef __TUI_H__
#define __TUI_H__

#include <stdarg.h>


void print_fmt(int x, int y, const char *fmt, ...);
void print_msg(const char *fmt, ...);
void print_ratio(int x, int y, float amt, float max);

#endif
